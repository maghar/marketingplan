var custom_values_robot = [1, 2, 3, 4, 5];
var custom_values_depos = [];
for (var i = 50; i <= 1500; i = i + 50) {
  custom_values_depos.push(i);
}


$("#robot-value-input").ionRangeSlider({
  grid: true,
  from: 2,
  //   to: 2,
  values: custom_values_robot
});

$("#depos-value-input").ionRangeSlider({
  grid: true,
  grid_num: 3,
  grid_snap: false,
  min: 0,
  max: 1500,
  step: 50,
  from: 250,
  //   to: 2,
  // values: custom_values_depos,

});

$(".calculator__range").addClass("irs-hidden-input");

document.getElementsByClassName('js-grid-text-0')[0].innerHTML = '50'