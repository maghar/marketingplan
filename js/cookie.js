

function getCookie(name) {
    var matches = document.cookie.match(
      new RegExp(
        "(?:^|; )" +
          name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") +
          "=([^;]*)"
      )
    );
    return matches ? decodeURIComponent(matches[1]) : false;
  }
  
  function checkUrl() {
    var url = new URL(window.location.href);
    var aff_id = url.searchParams.get("id");
    var result = false;
    if (aff_id) {
      result = aff_id;
    }
    return result;
  }
  
  function updateTelegramLinks(id) {
    var tel_links = document.getElementsByClassName("telegram-link__indeficator");
    var link = "https://t.me/snt_int_bot";
    for (var i = tel_links.length - 1; i >= 0; i--) {
      if (id) {
        tel_links[i].href = link + "?start=" + id;
      } else {
        tel_links[i].href = link;
      }
    }
  }
  
  //Range
  
  // Cockies and
  document.addEventListener("DOMContentLoaded", function(event) {
    if (!checkUrl()) {
      if (!getCookie("st_intelligence_id")) {
        updateTelegramLinks("");
      } else {
        //
        updateTelegramLinks(getCookie("st_intelligence_id"));
      }
    } else {
      document.cookie = "st_intelligence_id=" + checkUrl();
      updateTelegramLinks(checkUrl());
    }
    var uri = window.location.toString();
    if (uri.indexOf("?") > 0) {
      var clean_uri = uri.substring(0, uri.indexOf("?"));
      window.history.replaceState({}, document.title, clean_uri);
    }
  });
  