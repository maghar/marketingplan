var yourPackageTitle, packageComparisonsTitle, paymentValue, partnersNumber, isWindowSmall;

var initializationValue = function(){
  paymentValue = document.getElementById("depos-value-input").value;
  partnersNumber = document.getElementById("robot-value-input").value;
}
document.addEventListener("DOMContentLoaded", initializationValue);

var packs = [
  { name: "Tester", lvl1: 1, lvl2: 0.8, lvl3: 0.6, lvl4: 0.6, lvl5: 0, lvl6: 0, lvl7: 0, lvl8: 0, lvl9: 0, lvl10: 0, keys: 1},
  { name: "Advanced", lvl1: 1, lvl2: 0.7, lvl3: 0.7, lvl4: 0.6, lvl5: 0.5, lvl6: 0.4, lvl7: 0, lvl8: 0, lvl9: 0, lvl10: 0, keys: 1},
  { name: "Professional", lvl1: 1.1, lvl2: 0.8, lvl3: 0.7, lvl4: 0.6, lvl5: 0.7, lvl6: 0.4, lvl7: 0.3, lvl8: 0.2, lvl9: 0, lvl10: 0, keys: 2},
  { name: "Expert", lvl1: 1.2, lvl2: 0.9, lvl3: 0.7, lvl4: 0.7, lvl5: 0.8, lvl6: 0.5, lvl7: 0.3, lvl8: 0.2, lvl9: 0.1, lvl10: 0, keys: 2},
  { name: "Master", lvl1: 1.3, lvl2: 1, lvl3: 0.9, lvl4: 0.7, lvl5: 0.8, lvl6: 0.7, lvl7: 0.5, lvl8: 0.4, lvl9: 0.3, lvl10: 0.2, keys: 3}
];

var packsForEachFirst = function(i) {
  packs.forEach(function(item) {
    if (item.name === i) {
      var col11 =  (((Math.pow(partnersNumber, 1) * paymentValue) / 10) * item.lvl1).toFixed(2);
      var col21 = (((Math.pow(partnersNumber, 2) * paymentValue) / 10) * item.lvl2).toFixed(2);
      var col31  = (((Math.pow(partnersNumber, 3) * paymentValue) / 10) * item.lvl3).toFixed(2);
      var col41  = (((Math.pow(partnersNumber, 4) * paymentValue) / 10) * item.lvl4).toFixed(2);
      var col51  = (((Math.pow(partnersNumber, 5) * paymentValue) / 10) * item.lvl5).toFixed(2);
      var col61  = (((Math.pow(partnersNumber, 6) * paymentValue) / 10) * item.lvl6).toFixed(2);
      var col71  = (((Math.pow(partnersNumber, 7) * paymentValue) / 10) * item.lvl7).toFixed(2);
      var col81  = (((Math.pow(partnersNumber, 8) * paymentValue) / 10) * item.lvl8).toFixed(2);
      var col91  = (((Math.pow(partnersNumber, 9) * paymentValue) / 10) * item.lvl9).toFixed(2);
      var col101  = (((Math.pow(partnersNumber, 10) * paymentValue) / 10) * item.lvl10).toFixed(2);
      document.getElementById('column11').innerHTML = "$ " + col11.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column21').innerHTML = "$ " + col21.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column31').innerHTML = "$ " + col31.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column41').innerHTML = "$ " + col41.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column51').innerHTML = "$ " + col51.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column61').innerHTML = "$ " + col61.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column71').innerHTML = "$ " + col71.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column81').innerHTML = "$ " + col81.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column91').innerHTML = "$ " + col91.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column101').innerHTML = "$ " + col101.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column1key').innerHTML = item.keys;
      var sum1 = Number(col11) + Number(col21) + Number(col31) + Number(col41) + Number(col51) + Number(col61) + Number(col71) + Number(col81) + Number(col91) + Number(col101);
      document.getElementById('sumcol1').innerHTML = "$ " + (sum1).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " ");

    }
  });
};

var packsForEachSecond = function(i) {
  packs.forEach(function(item) {
    if (item.name === i) {
      var col12 = (((Math.pow(partnersNumber, 1) * paymentValue) / 10) * item.lvl1).toFixed(2);
      var col22 = (((Math.pow(partnersNumber, 2) * paymentValue) / 10) * item.lvl2).toFixed(2);
      var col32 = (((Math.pow(partnersNumber, 3) * paymentValue) / 10) * item.lvl3).toFixed(2);
      var col42 = (((Math.pow(partnersNumber, 4) * paymentValue) / 10) * item.lvl4).toFixed(2);
      var col52 = (((Math.pow(partnersNumber, 5) * paymentValue) / 10) * item.lvl5).toFixed(2);
      var col62 = (((Math.pow(partnersNumber, 6) * paymentValue) / 10) * item.lvl6).toFixed(2);
      var col72 = (((Math.pow(partnersNumber, 7) * paymentValue) / 10) * item.lvl7).toFixed(2);
      var col82 = (((Math.pow(partnersNumber, 8) * paymentValue) / 10) * item.lvl8).toFixed(2);
      var col92 = (((Math.pow(partnersNumber, 9) * paymentValue) / 10) * item.lvl9).toFixed(2);
      var col102 = (((Math.pow(partnersNumber, 10) * paymentValue) / 10) * item.lvl10).toFixed(2);
      document.getElementById('column12').innerHTML = "$ " + col12.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column22').innerHTML = "$ " + col22.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column32').innerHTML = "$ " + col32.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column42').innerHTML = "$ " + col42.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column52').innerHTML = "$ " + col52.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column62').innerHTML = "$ " + col62.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column72').innerHTML = "$ " + col72.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column82').innerHTML = "$ " + col82.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column92').innerHTML = "$ " + col92.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column102').innerHTML = "$ " + col102.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      document.getElementById('column2key').innerHTML = item.keys;
      var sum2 = Number(col12) + Number(col22) + Number(col32) + Number(col42) + Number(col52) + Number(col62) + Number(col72) + Number(col82) + Number(col92) + Number(col102);
      document.getElementById('sumcol2').innerHTML = "$ " + (sum2).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }
  });
};

// first column
var firstPackFunc = function() {
  var firstPack = document
    .getElementById("first-pack-packages")
    .getElementsByClassName("select-selected")[0].innerHTML;
  yourPackageTitle = document.getElementById(
    "your-package-title"
  ).innerHTML = firstPack;

  packsForEachFirst(yourPackageTitle);
  initializationSum()
};

var firstPackFunc2 = function() {
  var rate_value;
  if (document.getElementById('radio11').checked) {
      rate_value = document.getElementById('radio11').value;
  }
  if (document.getElementById('radio12').checked) {
      rate_value = document.getElementById('radio12').value;
    }
    if (document.getElementById('radio13').checked) {
      rate_value = document.getElementById('radio13').value;
    }
    if (document.getElementById('radio14').checked) {
      rate_value = document.getElementById('radio14').value;
    }
    if (document.getElementById('radio15').checked) {
      rate_value = document.getElementById('radio15').value;
    }

  yourPackageTitle = document.getElementById(
    "your-package-title"
  ).innerHTML = rate_value;

  packsForEachFirst(yourPackageTitle);
  initializationSum()
  
};

// second column
var secondPackFunc = function() {
  var secondPack = document
    .getElementById("second-pack-packages")
    .getElementsByClassName("select-selected")[0].innerHTML;
 
  packageComparisonsTitle = document.getElementById(
    "package-comparisons-title"
  ).innerHTML = secondPack;

  packsForEachSecond(packageComparisonsTitle);
  initializationSum()
};

var secondPackFunc2 = function() {
  var rate_value;
  if (document.getElementById('radio21').checked) {
      rate_value = document.getElementById('radio21').value;
  }
  if (document.getElementById('radio22').checked) {
      rate_value = document.getElementById('radio22').value;
    }
    if (document.getElementById('radio23').checked) {
      rate_value = document.getElementById('radio23').value;
    }
    if (document.getElementById('radio24').checked) {
      rate_value = document.getElementById('radio24').value;
    }
    if (document.getElementById('radio25').checked) {
      rate_value = document.getElementById('radio25').value;
    }

packageComparisonsTitle = document.getElementById(
  "package-comparisons-title"
).innerHTML = rate_value;

  packsForEachSecond(packageComparisonsTitle);
  initializationSum()

};


document.addEventListener("DOMContentLoaded", window.onresize = function(){
    document.addEventListener("DOMContentLoaded", firstPackFunc);
    document.addEventListener("click", firstPackFunc);
    document.addEventListener("DOMContentLoaded", secondPackFunc);
    document.addEventListener("click", secondPackFunc);
});

// switcher onchange
document.getElementById("depos-value-input").onchange = function() {
  var depos_value_input = this.value;
  paymentValue = this.value;
  firstPackFunc()
  secondPackFunc()
  document.getElementById("depos-value-output").value = depos_value_input;
  document.getElementById("depos-value-output-mobile").value = depos_value_input;
};
document.getElementById("robot-value-input").onchange = function() {
  var robot_value_input = this.value;
  partnersNumber = this.value;
  firstPackFunc()
  secondPackFunc()
  document.getElementById("robot-value-output").value = robot_value_input;
  document.getElementById("robot-value-output-mobile").value = robot_value_input;
};

var initializationSum = function(){
  var sumcol1 = document.getElementById('sumcol1').innerHTML;
  var sumOne = sumcol1.slice(1).replace(/\s/g,'');
  var sumcol2 = document.getElementById('sumcol2').innerHTML;
  var sumTwo = sumcol2.slice(1).replace(/\s/g,'');
  var total = (Number(sumTwo) - Number(sumOne))
  document.getElementById('lost-profit').innerHTML = "$ " + (total).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}
document.addEventListener("DOMContentLoaded", initializationSum);

